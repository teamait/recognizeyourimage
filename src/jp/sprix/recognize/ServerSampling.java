package jp.sprix.recognize;

import java.util.Random;

/**
 * データを受信するサーバの共通機能
 * 
 * @author root
 * 
 */
public class ServerSampling {
	private Random random = null;

	public ServerSampling(long seed) {
		random = new Random(seed);
	}

	public long getRandom() {
		return random.nextLong();
	}
}
