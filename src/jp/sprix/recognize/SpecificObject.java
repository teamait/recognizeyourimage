package jp.sprix.recognize;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import jp.sprix.jni.recognize.DetectImage;
import jp.sprix.jni.recognize.LoadDictionary;
import jp.sprix.jni.utils.FileUtils;

/**
 * Servlet implementation class RecognzeYourImage
 */
@WebServlet("/SpecificObject")
public class SpecificObject extends HttpServlet {
	static {
		System.loadLibrary("LoadDictionary");
	}

	private long nativeObject;
	private long nativeKeypoints;
	private long nativeCategory;

	private ServerSampling serverSamplint = null;

	private static final long serialVersionUID = 1L;

	public void init() throws ServletException {
		LoadDictionary ld = new LoadDictionary();
		String loadDicPath = ServerStatics.LOAD_HOME;
		nativeObject = ld.load(loadDicPath);
		nativeKeypoints = ld.loadKeyponts(loadDicPath);
		nativeCategory = ld.loadCategory(loadDicPath);
		System.out.println("load: " + nativeObject);
		ld.set(nativeObject, nativeKeypoints, nativeCategory);

		// 保存するファイル名を決定
		long randomSeed = System.currentTimeMillis() + nativeCategory;
		serverSamplint = new ServerSampling(randomSeed);

		// 辞書に対応したカテゴリーの取得ロジック
		ArrayList<String> categoryList = FileUtils.getArrayListFromFile(loadDicPath
				+ ServerStatics.CATEGORY_EXT);

		ServletContext sc = getServletContext();
		sc.setAttribute("dictionary", ld);
		sc.setAttribute("category", categoryList);
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SpecificObject() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletContext sc = getServletContext();
		String filepath = request.getParameter("filepath");
		LoadDictionary ld = (LoadDictionary) sc.getAttribute("dictionary");
		ArrayList<String> categoryList = (ArrayList<String>) sc.getAttribute("category");

		// 認識ロジック（tomcatの各スレッドから呼び出す）
		DetectImage di = new DetectImage();
		// int detect_index = di.detect(ld.getDictionary(), ld.getCategory(),
		// "/data/images/FILE0006.jpg", 256);

		// ver2 long nativeDictionary, long nativeCategory, long nativeKeypoints, String queryPath, long category_num
		String recognizeResult = di.detect(nativeObject, nativeCategory, nativeKeypoints, "/data/images/" + filepath, 256);
		System.out.println(recognizeResult);
		response.getWriter().write(recognizeResult);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 画像ファイルの取得
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(204800);
		factory.setRepository(new File("/var/tmp"));
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(1024 * 1024 * 10);
		upload.setFileSizeMax(1024 * 1024 * 10);
		List items;
		try {
			items = upload.parseRequest(request);

		} catch (FileUploadException e) {
			throw new ServletException(e);
		}

		// 保存するファイル名を決定
		String fileName = (Long.toString(Math.abs(serverSamplint.getRandom()))) + ".jpg";

		// 画像ファイルフラグ
		boolean putParam = false;
		boolean putImage = false;

		for (Object val : items) {
			FileItem item = (FileItem) val;
			if (item.isFormField()) {
				putParam = processFormField(item);
			} else {
				putImage = processUploadedFile(item, fileName);
			}
		}

		// フラグの確認
		if (!putImage || !putParam) {
			response.getWriter().write("[Error] request parameter is not correct.");
			return;
		}

		// サーブレットコンテナから取得する
		ServletContext sc = getServletContext();
		LoadDictionary ld = (LoadDictionary) sc.getAttribute("dictionary");
		ArrayList<String> categoryList = (ArrayList<String>) sc.getAttribute("category");

		// 認識ロジック（tomcatの各スレッドから呼び出す）
		DetectImage di = new DetectImage();
		String recognizeResult = di.detect(nativeObject, nativeCategory, nativeKeypoints, "/var/tmp/" + fileName, 256);
		System.out.println(recognizeResult);
		response.getWriter().write(recognizeResult);
	}

	private boolean processFormField(FileItem item) throws ServletException {
		String name = item.getFieldName();
		String value = item.getString();

		if (ServerStatics.REQUEST_NAME.equals(name) && ServerStatics.REQUEST_VALE.equals(value)) {
			return true;
		}
		return false;
	}

	private boolean processUploadedFile(FileItem item, String fileName) throws IOException,
			ServletException {
		File f = new File(item.getName());
		try {
			item.write(new File("/var/tmp", fileName));
		} catch (IOException e) {
			throw e;

		} catch (Exception e) {
			throw new ServletException(e);
		}

		return true;
	}

}
