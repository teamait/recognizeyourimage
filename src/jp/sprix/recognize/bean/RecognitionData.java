package jp.sprix.recognize.bean;

import javax.xml.bind.annotation.XmlRootElement;

import jp.sprix.recognize.ServerStatics;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@XmlRootElement
@JsonSerialize(include = Inclusion.NON_NULL)
public class RecognitionData {
	private String version = ServerStatics.SERVER_VERSION;

	// 認識成功:0、認識失敗:1、フィルタリング処理で排除:2、サーバエラー:3
	private int status;

	// 必要な場合メッセージを入れる
	private String message;

	// 認識カテゴリー
	private String category;

	// スコア
	private String score;

	// ホモグラフィー行列
	private String homography;

	// auto generate getter and setter
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getHomography() {
		return homography;
	}

	public void setHomography(String homography) {
		this.homography = homography;
	}

}
