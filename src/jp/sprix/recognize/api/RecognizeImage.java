package jp.sprix.recognize.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.sun.jersey.spi.resource.Singleton;

import jp.sprix.jni.recognize.DetectImage;
import jp.sprix.jni.recognize.LoadDictionary;
import jp.sprix.jni.utils.FileUtils;
import jp.sprix.recognize.ServerSampling;
import jp.sprix.recognize.ServerStatics;
import jp.sprix.recognize.bean.RecognitionData;

@Singleton
@Path("/image")
public class RecognizeImage extends HttpServlet {
	private static final long serialVersionUID = 856271730322953368L;

	@Context
	ServletContext context;

	@Context
	private HttpServletRequest mHttpServletRequest;

	static {
		System.loadLibrary("LoadDictionary");
	}

	private long nativeObject;
	private long nativeKeypoints;
	private long nativeCategory;

	private ServerSampling serverSamplint = null;

	@POST
	@Path("json")
	@Produces({ MediaType.APPLICATION_JSON })
	public RecognitionData recognizeImage() {
		// 初回のみ動作
		if (context.getAttribute("dictionary") == null
				|| !context.getAttribute("resource").equals("1")) {
			LoadDictionary ld = new LoadDictionary();
			String loadDicPath = ServerStatics.LOAD_HOME;
			nativeObject = ld.load(loadDicPath);
			nativeKeypoints = ld.loadKeyponts(loadDicPath);
			nativeCategory = ld.loadCategory(loadDicPath);
			System.out.println("load: " + nativeObject);
			ld.set(nativeObject, nativeKeypoints, nativeCategory);

			// 保存するファイル名を決定
			long randomSeed = System.currentTimeMillis() + nativeCategory;
			serverSamplint = new ServerSampling(randomSeed);

			// 辞書に対応したカテゴリーの取得ロジック
			ArrayList<String> categoryList = FileUtils.getArrayListFromFile(loadDicPath
					+ ServerStatics.CATEGORY_EXT);

			context.setAttribute("dictionary", ld);
			context.setAttribute("category", categoryList);
			context.setAttribute("resource", "1");
		}

		// 画像ファイルの取得
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(204800);
		factory.setRepository(new File("/var/tmp"));
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(1024 * 1024 * 10);
		upload.setFileSizeMax(1024 * 1024 * 10);
		List items;
		try {
			items = upload.parseRequest(mHttpServletRequest);

		} catch (FileUploadException e) {
			RecognitionData recognitionData = new RecognitionData();
			recognitionData.setStatus(ServerStatics.RECOGNIZE_SERVER_ERROR);
			recognitionData.setMessage("[Error] Can not file Upload. " + e.getMessage());
			return recognitionData;
		}

		// 保存するファイル名を決定
		String fileName = (Long.toString(Math.abs(serverSamplint.getRandom()))) + ".jpg";

		// 画像ファイルフラグ
		boolean putParam = false;
		boolean putImage = false;

		for (Object val : items) {
			FileItem item = (FileItem) val;
			if (item.isFormField()) {
				try {
					putParam = processFormField(item);
				} catch (ServletException e) {
					RecognitionData recognitionData = new RecognitionData();
					recognitionData.setStatus(ServerStatics.RECOGNIZE_SERVER_ERROR);
					recognitionData.setMessage("[Error] Parameter Failed. " + e.getMessage());
					return recognitionData;
				}
			} else {
				try {
					putImage = processUploadedFile(item, fileName);
				} catch (IOException | ServletException e) {
					RecognitionData recognitionData = new RecognitionData();
					recognitionData.setStatus(ServerStatics.RECOGNIZE_SERVER_ERROR);
					recognitionData.setMessage("[Error] Image Failed. " + e.getMessage());
					return recognitionData;
				}
			}
		}

		// フラグの確認
		if (!putImage || !putParam) {
			// [Error] request parameter is not correct.
			// TODO 例外処理
		}

		// サーブレットコンテナから取得する
		LoadDictionary ld = (LoadDictionary) context.getAttribute("dictionary");
		ArrayList<String> categoryList = (ArrayList<String>) context.getAttribute("category");

		// 認識ロジック（tomcatの各スレッドから呼び出す）
		DetectImage di = new DetectImage();
		String recognizeResult = di.detect(nativeObject, nativeCategory, nativeKeypoints,
				ServerStatics.SERVER_IMAGE_HOME + fileName, 256);
		return parseRecognitionResult(recognizeResult);
	}

	private boolean processFormField(FileItem item) throws ServletException {
		String name = item.getFieldName();
		String value = item.getString();

		if (ServerStatics.REQUEST_NAME.equals(name) && ServerStatics.REQUEST_VALE.equals(value)) {
			return true;
		}
		return false;
	}

	private boolean processUploadedFile(FileItem item, String fileName) throws IOException,
			ServletException {
		File f = new File(item.getName());
		try {
			item.write(new File("/var/tmp", fileName));
		} catch (IOException e) {
			throw e;

		} catch (Exception e) {
			throw new ServletException(e);
		}

		return true;
	}

	private RecognitionData parseRecognitionResult(String result) {
		if (result == null || result.length() == 0) {
			RecognitionData recognitionData = new RecognitionData();
			recognitionData.setStatus(ServerStatics.RECOGNIZE_FAILED);
			recognitionData.setMessage("[Error] Recognition Result is null or empty.");
			return recognitionData;
		}

		String[] resultEntry = result.split("\t");
		if (resultEntry.length < 4) {
			RecognitionData recognitionData = new RecognitionData();
			recognitionData.setStatus(ServerStatics.RECOGNIZE_FAILED);
			recognitionData.setMessage("[Error] Recognition Result's Entry Count. " + result);
			return recognitionData;
		}

		String elapsedTime = resultEntry[0];
		String category = resultEntry[1];
		String score = resultEntry[2];
		String homography = resultEntry[3];

		// categoryのまわりの余分なCharacterを排除
		if(category != null){
			category = category.replaceAll("\r", "");
		}
		
		RecognitionData recognitionData = new RecognitionData();
		recognitionData.setCategory(category);
		recognitionData.setScore(score);
		recognitionData.setHomography(homography);
		recognitionData.setStatus(ServerStatics.RECOGNIZE_SUCCESS);

		return recognitionData;
	}
}
