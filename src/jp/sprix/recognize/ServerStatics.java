package jp.sprix.recognize;

public class ServerStatics {
	// load home
	public static final String LOAD_HOME = "/etc/sprix/dictionary/dic_text_sprix";

	// dictionary settings
	public static final String CATEGORY_EXT = "_category";

	public static final String REQUEST_NAME = "image";

	public static final String REQUEST_VALE = "specific";

	// server image home
	public static final String SERVER_IMAGE_HOME = "/var/tmp/";

	// recognize response code
	public static final int RECOGNIZE_SUCCESS = 0;
	public static final int RECOGNIZE_FAILED = 1;
	public static final int RECOGNIZE_FILTER = 2;
	public static final int RECOGNIZE_SERVER_ERROR = 3;

	// server version
	public static final String SERVER_VERSION = "Ver.0.9.0";
}
